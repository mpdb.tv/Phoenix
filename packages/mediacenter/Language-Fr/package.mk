################################################################################
#      This file is part of OpenELEC - http://www.openelec.tv
#      Copyright (C) 2009-2016 Stephan Raue (stephan@openelec.tv)
#
#  OpenELEC is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 2 of the License, or
#  (at your option) any later version.
#
#  OpenELEC is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with OpenELEC.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

PKG_NAME="Language-Fr"
PKG_VERSION="3.0.13"
PKG_ARCH="any"
PKG_LICENSE="prop."
PKG_SITE="https://kodi.tv/"
#PKG_URL="http://mirrors.kodi.tv/addons/krypton/resource.language.fr_fr/resource.language.fr_fr-$PKG_VERSION.zip"
PKG_URL="http://ftp.halifax.rwth-aachen.de/xbmc/addons/krypton/resource.language.fr_fr/resource.language.fr_fr-$PKG_VERSION.zip"
PKG_SOURCE_DIR="resource.language.fr_fr"
PKG_DEPENDS_TARGET="toolchain"
PKG_SECTION="resource.language"
PKG_SHORTDESC="French language pack"
PKG_LONGDESC="French version of all texts used in Kodi."

PKG_IS_ADDON="no"
PKG_AUTORECONF="no"

make_target() {
  : # nop
}

makeinstall_target() {
  : # nop
}

post_install() {
  mkdir -p $INSTALL/usr/share/kodi/addons/resource.language.fr_fr
#    rm -f $BUILD/$PKG_NAME-$PKG_VERSION/.libreelec-unpack
    cp -r $BUILD/$PKG_NAME-$PKG_VERSION/* $INSTALL/usr/share/kodi/addons/resource.language.fr_fr/
    rm -f $INSTALL/usr/share/kodi/addons/resource.language.fr_fr/.libreelec-unpack
}
