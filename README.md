# Phoenix

Phoenix is a fork of [LibreELEC](https://libreelec.tv/), a 'Just enough OS' Linux distribution for running the award-winning [Kodi](http://kodi.tv) software on popular mediacentre hardware. LibreELEC is a conservative fork of the popular [OpenELEC](http://openelec.tv) project with a stronger focus on pre-release testing and post-release change management. Further information on the project can be found on the [MPDB website](https://mpdb.tv).

**Issues & Support**

Please report issues and ask questions via the [MPDB forum](https://forum.mpdb.tv).

**License**

Phoenix original code is released under [GPLv2](http://www.gnu.org/licenses/gpl-2.0.html).

**Copyright**

As Phoenix includes code from many upstream projects it includes many copyright owners. Phoenix makes NO claim of copyright on any upstream code. However all original Phoenix authored code is copyright mpdb.tv. For a complete copyright list please checkout the source code to examine license headers. Unless expressly stated otherwise all code submitted to the Phoenix project (in any form) is licensed under [GPLv2](http://www.gnu.org/licenses/gpl-2.0.html) and copyright is donated to mpdb.tv. This approach allows the project to stay manageable in the long term by giving us freedom to maintain the code as part of the whole without the management overhead of preserving contact with every submitter, e.g. GPLv3. You are absolutely free to retain copyright. To retain copyright simply add a copyright header to each submitted code page. If you submit code that is not your own work it is your responsibility to place a header stating the copyright.
